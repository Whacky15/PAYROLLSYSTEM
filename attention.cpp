#include "attention.h"
#include "ui_attention.h"


Attention::Attention(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Attention)

{
    ui->setupUi(this);

     connect(ui->comboBox, SIGNAL(currentTextChanged(QString)), this, SLOT(displayAttendance()));
    dbname.setDatabaseName("C:/Users/Kate/Documents/DATABASE payroll/employee.db");



}


Attention::~Attention()
{
    delete ui;
}

void Attention::displayAttendance(){
    QString date = ui->comboBox->currentText();

   if (dbname.open()) {

        tableview_tbm = new QSqlTableModel(this, dbname);
        tableview_tbm->setEditStrategy(QSqlTableModel::OnManualSubmit);
        tableview_tbm->setTable(date);
        tableview_tbm->select();
        ui->tableView->setModel(tableview_tbm);

    dbname.close();
}
}

void Attention::on_btnSV_clicked()
{
    dbname.open();
    tableview_tbm->submitAll();
    dbname.close();
}

void Attention::on_btnBK_clicked()
{
    this->close();
}

void Attention::on_btnRT_clicked()
{
    dbname.open();
    tableview_tbm->revertAll();
    dbname.close();
}
