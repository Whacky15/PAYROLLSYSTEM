#ifndef REGISTER_H
#define REGISTER_H

#include <QDialog>
#include<QtSql>
#include <QMessageBox>
namespace Ui {
class REGISTER;
}

class REGISTER : public QDialog
{
    Q_OBJECT

public:
    explicit REGISTER(QWidget *parent = nullptr);
    ~REGISTER();

private slots:
    void on_btnBack_clicked();
    void on_btnAdd_clicked();

private:
    Ui::REGISTER *ui;
    QSqlDatabase dbname= QSqlDatabase::database("login_conn");
};

#endif // REGISTER_H
