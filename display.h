#ifndef DISPLAY_H
#define DISPLAY_H

#include <QDialog>
#include<QtSql>
#include <QMessageBox>
namespace Ui {
class DISPLAY;
}

class DISPLAY : public QDialog
{
    Q_OBJECT

public:
    explicit DISPLAY(QWidget *parent = nullptr);
    ~DISPLAY();

public slots:
    void displayTable();

private slots:
    void on_pushButton_clicked();



private:
    Ui::DISPLAY *ui;
    QSqlDatabase dbname= QSqlDatabase::database("login_conn");
};

#endif // DISPLAY_H
