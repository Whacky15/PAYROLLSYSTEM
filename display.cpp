#include "display.h"
#include "ui_display.h"

DISPLAY::DISPLAY(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DISPLAY)
{
    ui->setupUi(this);
    ui->tableView->verticalHeader()->setVisible(false);
    dbname.setDatabaseName("C:/Users/Kate/Documents/DATABASE payroll/employee.db");

}

DISPLAY::~DISPLAY()
{
    delete ui;
}

void DISPLAY::displayTable() {
    dbname.removeDatabase("display_conn");
    if (dbname.open()) {
        QSqlQueryModel *tableview_qrm = new QSqlQueryModel;
        tableview_qrm->setQuery(dbname.exec("SELECT * FROM Employees"));
        ui->tableView->setModel(tableview_qrm);
        dbname.close();

    }
    else {
        QMessageBox::warning(this, "SQL Open Error", "An error occured while opening the database.");
    }




}

void DISPLAY::on_pushButton_clicked()
{
    this->close();

}

