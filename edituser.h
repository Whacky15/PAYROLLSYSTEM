#ifndef EDITUSER_H
#define EDITUSER_H

#include <QDialog>
#include<QtSql>
#include <QMessageBox>

namespace Ui {
class EDITUSER;
}

class EDITUSER : public QDialog
{
    Q_OBJECT

public:
    explicit EDITUSER(QWidget *parent = nullptr);
    ~EDITUSER();

private slots:
    void displayTable();




   void on_btnDelete_clicked();
    void on_btnBack_clicked();

    void on_btnSave_clicked();

    void on_btnRestore_clicked();

private:
    Ui::EDITUSER *ui;
    QSqlDatabase dbname= QSqlDatabase::database("login_conn");
    QSqlTableModel *tableview_tbm;
};

#endif // EDITUSER_H
