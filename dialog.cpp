#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    dbname.setDatabaseName("C:/Users/Kate/Documents/DATABASE payroll/employee.db");
    dbname.open();
    dbname.close();

    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    QString id = ui->ledID->text();
    QString first_name = ui->ledFirstName->text();
    QString middle_name = ui->ledMiddleName->text();
    QString last_name = ui->ledLastName->text();
    QString email = ui->ledEmail->text();
    QString dob = ui->dtedDOB->text();
    QString address = ui->ptxtAddress->toPlainText();
    QString admitted = ui->dtedAdmitted->text();
    QString role = ui->cbxRole->currentText();
    QString gross_perday = ui->ledGPD->text();
    if (dbname.open()) {
        dbname.transaction();
        QSqlQuery addemployee_qry(dbname);
        qDebug().noquote() << "Executing SQL query: INSERT INTO Employees VALUES (" + id + ", \"" + first_name + "\", \"" + middle_name + "\", \"" + last_name + "\", \"" + email + "\", \"" + dob + "\", \"" + address + "\", \"" + admitted + "\", \"" + role + "\", " + gross_perday + ")";
        if (addemployee_qry.exec("UPDATE Employees VALUES (" + id + ", \"" + first_name + "\", \"" + middle_name + "\", \"" + last_name + "\", \"" + email + "\", \"" + dob + "\", \"" + address + "\", \"" + admitted + "\", \"" + role + "\"," + gross_perday + ")")) {
            dbname.commit();
            QMessageBox::information(this, "SQL Query Success", "Your data was successfully inserted into the database.");
        }
        else {
            dbname.rollback();
            qDebug().noquote() << addemployee_qry.lastError();
            QMessageBox::warning(this, "SQL Query Error", "An error occured while inserting data into the database. Your query was rolled back.");
        }
        dbname.close();
    }
    else {
        QMessageBox::warning(this, "SQL Open Error", "An error occured while opening the database. No queries were executed.");
    }


}

void Dialog::on_pushButton_2_clicked()
{

}

