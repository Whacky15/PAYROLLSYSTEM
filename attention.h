#ifndef ATTENTION_H
#define ATTENTION_H

#include <QDebug>
#include <QDialog>
#include <QtSql>

namespace Ui {
class Attention;
}

class Attention : public QDialog
{
    Q_OBJECT

public:
    explicit Attention(QWidget *parent = nullptr);
    ~Attention();
private slots:

    void displayAttendance();

    void on_btnSV_clicked();

    void on_btnBK_clicked();

    void on_btnRT_clicked();

private:
    Ui::Attention *ui;
    QSqlDatabase dbname = QSqlDatabase::database("login_conn");
    QSqlTableModel *tableview_tbm;
};

#endif // ATTENTION_H

