#include "receipt.h"
#include "ui_receipt.h"

Receipt::Receipt(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Receipt)
{
    ui->setupUi(this);
     dbname.setDatabaseName("C:/Users/Kate/Documents/DATABASE payroll/employee.db");


}

Receipt::~Receipt()
{
    delete ui;
}


void Receipt::on_btnback_clicked()
{
   this->close();
}

void Receipt::showInfo(QString cum_salary, QString sss, QString phealth, QString ded_sum, QString net_salary, QString lastn, QString firstn, QString id, QString grosss) {
    ui->plainTextEdit->clear();
    ui->plainTextEdit->appendPlainText("----------PAYROLL-----------");
    ui->plainTextEdit->appendPlainText("ID:                 " + grosss );
    ui->plainTextEdit->appendPlainText("Firstname:          " + firstn);
    ui->plainTextEdit->appendPlainText("Lastname:           " + lastn);
    ui->plainTextEdit->appendPlainText("Gross:              " + id);
    ui->plainTextEdit->appendPlainText("----------------------------");
    ui->plainTextEdit->appendPlainText(" Cumulative Salary: " + cum_salary );
    ui->plainTextEdit->appendPlainText(" SSS:               " + sss );
    ui->plainTextEdit->appendPlainText(" PhilHealth:        " + phealth );
    ui->plainTextEdit->appendPlainText(" total Deduction:   " + ded_sum );
    ui->plainTextEdit->appendPlainText("----------------------------");
    ui->plainTextEdit->appendPlainText(" Net Salary:        " + net_salary );

    ui->plainTextEdit->appendPlainText("");
    ui->plainTextEdit->appendPlainText("");
    ui->plainTextEdit->appendPlainText("         THANK YOU   ");


//    if (ui->plainTextEdit->toPlainText().isEmpty()){
       //ui->plainTextEdit->setPlainText("PH is "+ ph +", CMS is"+ cms +", SSS is"+ sss +", NS is" + ns +", td is "+td);
//}

}

void Receipt::on_btndone_clicked()
{
    this->close();
}
