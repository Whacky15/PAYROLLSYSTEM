#ifndef RECEIPT_H
#define RECEIPT_H

#include <QDialog>
#include <QtSql>



namespace Ui {
class Receipt;
}

class Receipt : public QDialog
{
    Q_OBJECT

public:
    explicit Receipt(QWidget *parent = nullptr);
    ~Receipt();

private slots:
    void showInfo(QString, QString, QString, QString, QString, QString, QString, QString, QString);

    void on_btnback_clicked();

    void on_btndone_clicked();

private:
    Ui::Receipt *ui;
    QSqlDatabase dbname= QSqlDatabase::database("login_conn");
    Receipt *rct;


};

#endif // RECEIPT_H
