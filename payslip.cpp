﻿#include "payslip.h"
#include "ui_payslip.h"

PAYSLIP::PAYSLIP(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PAYSLIP)
{
    ui->setupUi(this);
    dbname.setDatabaseName("C:/Users/Kate/Documents/DATABASE payroll/employee.db");
    connect(ui->comboBox, SIGNAL(currentTextChanged(QString)), this, SLOT(displaySalaryInfo()));
    connect(ui->tableView, SIGNAL(clicked(QModelIndex)), this, SLOT (displaySalaryInfo()));
    displayTable();
}

PAYSLIP::~PAYSLIP()
{
    delete ui;
}

void PAYSLIP::displaySalaryInfo(){
    QString curr_date = ui->comboBox->currentText();
    QModelIndex item_index = ui->tableView->currentIndex();
    QString item_id = item_index.sibling(item_index.row(),0).data().toString();
    qDebug().noquote() << "Current DATE" << curr_date;
    qDebug().noquote() << "Item ID" << item_id;


    if (curr_date != "Choose a date" && isClicked == true) {


    dbname.open();
    QSqlQuery gross_qry(dbname);
    gross_qry.exec("SELECT GrossPerDay FROM Employees WHERE ID = " + item_id + "");
    gross_qry.next();
    int gross = gross_qry.value(0).toInt();
    QSqlQuery june_qry(dbname);
    june_qry.exec("SELECT * FROM \"" + curr_date +"\"");
    june_qry.next();
    QSqlQuery id_qry(dbname);
    id_qry.exec("SELECT ID FROM Employees");
    id_qry.next();
    QString id = id_qry.value(0).toString();
    QSqlQuery firstn_qry(dbname);
    firstn_qry.exec("SELECT FirstName FROM Employees");
    firstn_qry.next();
    QString firstn = firstn_qry.value(0).toString();
    QSqlQuery lastn_qry(dbname);
    lastn_qry.exec("SELECT LastName FROM Employees");
    lastn_qry.next();
    QString lastn = lastn_qry.value(0).toString();



     double total = 0;
     double total_deductions = 0;
     int late = 0;
     int absent = 0;
     int leave = 0;
     int present = 0;

     int grosss = gross/2;




     for (int i = 1; i<16; i++){
     QString dayentry = june_qry.value(i).toString();
        qDebug()<<june_qry.value(i).toString();

        if (dayentry == "P"){
            total +=  grossFullPay(gross);
            present += 1;



        }
        else if (dayentry =="L"){
            total += grossFullPay(gross) - (0.02 * grossFullPay(gross));
            qDebug() << grossFullPay(gross) - (0.02 * grossFullPay(gross));
            total_deductions += 0.02 * grossFullPay(gross);
            late += 1;
        }
        else if (dayentry =="A"){
            total += 0;
            total_deductions += grossFullPay(gross);
            absent += 1;
        }
        else if (dayentry =="l"){
            total += grossFullPay(gross) - (0.01 * grossFullPay(gross));
            qDebug() << grossFullPay(gross) - (0.01 * grossFullPay(gross));
            total_deductions += 0.01 * grossFullPay(gross);
            leave += 1;
        }
        else {
            qDebug().noquote()<< "Illegal Input";

        }
     }

     ui->ledlate->setText(QString::number(late));
     ui->ledabsent->setText(QString::number(absent));
     ui->ledleave->setText(QString::number(leave));
     ui->ledpresent->setText(QString::number(present));
     ui->ledid->setText(id);
     ui->ledfn->setText(firstn);
     ui->ledgrs->setText(QString::number(grosss));
     ui->ledln->setText(lastn);
     qDebug().noquote() << "lastn:" << lastn;





     dbname.exec();
     qDebug().noquote()<< "total_salary: " << total;
     qDebug().noquote() << "total_deductions: " << total_deductions;

     dbname.close();

     ui->ledCMS->setText(QString::number(total));
     double philhealth = total * 0.0275;
     ui->ledPH->setText(QString::number(philhealth));
     double sss = total * 0.11;
     ui->ledSSS->setText(QString::number(sss));
     double totald = total_deductions + philhealth + sss;
     ui->ledTD->setText(QString::number(totald));
     double nets = total - philhealth - sss;
     ui->lineEdit_5->setText(QString::number(nets));

     }
    else {
        ui->ledCMS->setText("N/A");
        ui->ledPH->setText("N/A");
        ui->ledSSS->setText("N/A");
        ui->ledTD->setText("N/A");
        ui->lineEdit_5->setText("N/A");

    }
}
void PAYSLIP::on_pushButton_clicked()
{
    this->close();
}
int PAYSLIP::grossFullPay(int gross){

    int fullpay = (gross/2)/15;
    return fullpay;
}
void PAYSLIP::displayTable() {
    if (dbname.open()) {
        QSqlQueryModel *tableview_qrm = new QSqlQueryModel;
        tableview_qrm->setQuery(dbname.exec("SELECT ID, LastName, FirstName, grossperday FROM Employees;"));
        ui->tableView->setModel(tableview_qrm);
        dbname.close();
    }
}

void PAYSLIP::makeTrue(bool &falseVar) {
    qDebug().noquote() << "isClicked: " << isClicked;
    falseVar = true;
    qDebug().noquote() << "isClicked: " << isClicked;
}

void PAYSLIP::on_tableView_clicked(const QModelIndex &)
{
    makeTrue(isClicked);
}

void PAYSLIP::on_btnprint_clicked()
{
    rct = new Receipt(this);
    rct->show();
    rct->setModal(true);
    connect(this, SIGNAL(showInfo(QString, QString, QString, QString, QString, QString, QString, QString, QString)), rct, SLOT(showInfo(QString, QString, QString, QString, QString, QString, QString, QString, QString)) );

    QString cum_salary = ui->ledCMS->text();
    QString sss = ui->ledSSS->text();
    QString phealth = ui->ledPH->text();
    QString ded_sum = ui->ledTD->text();
    QString net_salary = ui->lineEdit_5->text();
    QString id = ui->ledid->text();
    QString firstn = ui->ledfn->text();
    QString lastn = ui->ledln->text();
    QString grosss = ui->ledgrs->text();
    emit showInfo(cum_salary, sss, phealth, ded_sum, net_salary, lastn, firstn, grosss, id);
}


