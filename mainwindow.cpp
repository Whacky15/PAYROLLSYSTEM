#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionREGISTER_triggered()
{
 rs = new REGISTER(this);
 rs->setModal(true);
 rs->show();
}

void MainWindow::on_actionEDIT_USER_triggered()
{
    eu = new EDITUSER(this);
    eu->show();
    eu->setModal(true);
    connect(this, SIGNAL(displayTable_edit()), eu, SLOT(displayTable()));
    emit displayTable_edit();

}

void MainWindow::on_actionDISPLAY_USER_triggered()
{
    dp = new DISPLAY(this);
    dp->show();
    dp->setModal(true);
    connect(this, SIGNAL(displayTable()), dp, SLOT(displayTable()));
    emit displayTable();
}

void MainWindow::on_actionPAYSLIP_triggered()
{
    ps = new PAYSLIP(this);
    ps->show();
    ps->setModal(true);

}

void MainWindow::on_actionATTENDACE_triggered()
{
    atn = new Attention(this);
    atn->show();
    atn->setModal(true);
}
