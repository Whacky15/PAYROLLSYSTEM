#include "edituser.h"
#include "ui_edituser.h"

EDITUSER::EDITUSER(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EDITUSER)
{
    ui->setupUi(this);
    dbname.setDatabaseName("C:/Users/Kate/Documents/DATABASE payroll/employee.db");
}

EDITUSER::~EDITUSER()
{
    delete ui;
}

void EDITUSER::displayTable() {
    if (dbname.open()) {
        tableview_tbm = new QSqlTableModel(this, dbname);
        tableview_tbm->setEditStrategy(QSqlTableModel::OnManualSubmit);
        tableview_tbm->setTable("Employees");
        tableview_tbm->select();
        ui->tableView->setModel(tableview_tbm);

        dbname.close();


    }
    else {
        QMessageBox::warning(this, "SQL Open Error", "An error occured while opening the database.");
    }
}

void EDITUSER::on_btnDelete_clicked()
{
    dbname.open();
    QModelIndex tv_index = ui->tableView->currentIndex();
    QString id = tv_index.data(Qt::DisplayRole).toString();
    dbname.exec("DELETE FROM Employees WHERE ID = " + id);
    QSqlQueryModel *tableview_qrm = new QSqlQueryModel;
    tableview_qrm->setQuery(dbname.exec("SELECT * FROM Employees"));
    ui->tableView->setModel(tableview_qrm);
    dbname.close();
}

void EDITUSER::on_btnBack_clicked()
{
    this->close();
}

void EDITUSER::on_btnSave_clicked()
{
    dbname.open();
    tableview_tbm->submitAll();
    dbname.close();
}

void EDITUSER::on_btnRestore_clicked()
{
    dbname.open();
    tableview_tbm->revertAll();
    dbname.close();
}
