#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <register.h>
#include <display.h>
#include <edituser.h>
#include <payslip.h>
#include <QMainWindow>
#include <dialog.h>
#include <QDialog>
#include <attention.h>
#include <receipt.h>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionREGISTER_triggered();

    void on_actionEDIT_USER_triggered();

    void on_actionDISPLAY_USER_triggered();

    void on_actionPAYSLIP_triggered();

    void on_actionATTENDACE_triggered();

signals:
    void displayTable();
    void displayTable_edit();


private:
    Ui::MainWindow *ui;
    REGISTER *rs;
    DISPLAY *dp;
    EDITUSER *eu;
    PAYSLIP *ps;
    Dialog *dg;
    Attention *atn;
    Receipt *rct;
};

#endif // MAINWINDOW_H
