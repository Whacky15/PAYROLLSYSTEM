#ifndef PAYSLIP_H
#define PAYSLIP_H

#include <QDialog>
#include<QtSql>
#include<receipt.h>





namespace Ui {
class PAYSLIP;
}

class PAYSLIP : public QDialog
{
    Q_OBJECT

signals:
    void showInfo(QString, QString, QString, QString, QString, QString, QString, QString, QString);


public:
    explicit PAYSLIP(QWidget *parent = nullptr);
    ~PAYSLIP();

private slots:
    void on_pushButton_clicked();
    void displaySalaryInfo();
    int grossFullPay(int);
    void displayTable();
    void makeTrue(bool &);




    void on_tableView_clicked(const QModelIndex &index);

    void on_btnprint_clicked();

private:
    Ui::PAYSLIP *ui;
    QSqlDatabase dbname= QSqlDatabase::database("login_conn");
    bool isClicked = false;
    Receipt *rct;
    PAYSLIP *ps;

};

#endif // PAYSLIP_H
