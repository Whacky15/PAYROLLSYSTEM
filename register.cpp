#include "register.h"
#include "ui_register.h"

REGISTER::REGISTER(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::REGISTER)
{
    ui->setupUi(this);
    dbname.setDatabaseName("C:/Users/Kate/Documents/DATABASE payroll/employee.db");
    dbname.open();
    dbname.close();
}

REGISTER::~REGISTER()
{
    delete ui;
}

void REGISTER::on_btnAdd_clicked()
{
    QString id = ui->ledID->text();
    QString first_name = ui->ledFirstName->text();
    QString middle_name = ui->ledMiddleName->text();
    QString last_name = ui->ledLastName->text();
    QString email = ui->ledEmail->text();
    QString dob = ui->dtedDOB->text();
    QString address = ui->ptxtAddress->toPlainText();
    QString admitted = ui->dtedAdmitted->text();
    QString role = ui->cbxRole->currentText();
    QString gross_perday = ui->ledGPD->text();


    if (dbname.open()) {
        dbname.transaction();
        QSqlQuery addemployee_qry(dbname);
        qDebug().noquote() << "Executing SQL query: INSERT INTO Employees VALUES (" + id + ", \"" + first_name + "\", \"" + middle_name + "\", \"" + last_name + "\", \"" + email + "\", \"" + dob + "\", \"" + address + "\", \"" + admitted + "\", \"" + role + "\", " + gross_perday + ")";
        if (addemployee_qry.exec("INSERT INTO Employees VALUES (" + id + ", \"" + first_name + "\", \"" + middle_name + "\", \"" + last_name + "\", \"" + email + "\", \"" + dob + "\", \"" + address + "\", \"" + admitted + "\", \"" + role + "\"," + gross_perday + ")")) {
            dbname.commit();
            QMessageBox::information(this, "SQL Query Success", "Your data was successfully inserted into the database.");
        }
        else {
            dbname.rollback();
            qDebug().noquote() << addemployee_qry.lastError();
            QMessageBox::warning(this, "SQL Query Error", "An error occured while inserting data into the database. Your query was rolled back.");
        }
        dbname.close();
    }

    if (dbname.open()) {
        dbname.transaction();
        QSqlQuery addemployee_qry(dbname);
        qDebug().noquote()<< "INSERT INTO \"June 10\" VALUES (" + id + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
        if (addemployee_qry.exec("INSERT INTO \"June 10\" VALUES (" + id + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)")) {

            dbname.commit();
        }
        else {
            dbname.rollback();
        }
        dbname.close();
    }
    if (dbname.open()) {
        dbname.transaction();
        QSqlQuery addemployee_qry(dbname);
        qDebug().noquote()<< "INSERT INTO \"June 25\" VALUES (" + id + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
        if (addemployee_qry.exec("INSERT INTO \"June 25\" VALUES (" + id + ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)")) {
            dbname.commit();
        }
        else {
            dbname.rollback();
        }
        dbname.close();
    }


}

void REGISTER::on_btnBack_clicked()
{
    this->close();
}
