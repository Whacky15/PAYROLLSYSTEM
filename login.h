#ifndef LOGIN_H
#define LOGIN_H

#include <QMainWindow>
#include<QtSql>
#include<QDebug>
#include <mainwindow.h>
#include <qmessagebox.h>


QT_BEGIN_NAMESPACE
namespace Ui { class LOGIN; }
QT_END_NAMESPACE

class LOGIN : public QMainWindow
{
    Q_OBJECT

public:
    LOGIN(QWidget *parent = nullptr);
    ~LOGIN();

private slots:

    void on_btnExit_clicked();

    void on_btnLogin_clicked();

private:
    Ui::LOGIN *ui;
    QSqlDatabase dbname = QSqlDatabase::addDatabase("QSQLITE", "login_conn");
    MainWindow mw;



};
#endif // LOGIN_H
